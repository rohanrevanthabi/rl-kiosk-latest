// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {             //dev.runloyal
  production: false,
  basePath: "https://dev.runloyal.com/services",
  userService: "https://dev.runloyal.com/services/user-service",
  notificationService: "https://dev.runloyal.com/services/notification-service",
  calendarService: "https://dev.runloyal.com/services/calendar-service",
  messageService: "https://dev.runloyal.com/services/message-service",
  paymentReturnUrl: "https://dev.runloyal.com/suitepaws/thankyou.html",
  // tenantKey: "dogsrus",
  // tenantKey: "paw-palace",
  // tenantKey: "",
  tenantKey: "suitepaws",
  // tenantKey: "amopetcare",
  // tenantKey: "dogspaw",
  // tenantKey: "thehoundry",
  // tenantKey: "pupncuts",
  // tenantKey: "dogkingdom",
  // tenantKey: "runloyalpetresort",
  // tenantKey: "themuddymutt", 
  // tenantKey: "fidofido-alpharetta",
  applicationKey: "iTrust-PawOps",
  version: "1.29",
};

// export const environment = {             //uat.runloyal
//   production: false,
//   basePath: "https://uat.runloyal.com/services",
//   userService: "https://uat.runloyal.com/services/user-service",
//   notificationService: "https://uat.runloyal.com/services/notification-service",
//   calendarService: "https://uat.runloyal.com/services/calendar-service",
//   messageService: "https://uat.runloyal.com/services/message-service",
//   paymentReturnUrl: "https://uat.runloyal.com/suitepaws/thankyou.html",
//   // tenantKey: "dogsrus",
//   // tenantKey: "paw-palace",
//   // tenantKey: "",
//   tenantKey: "suitepaws",
//   // tenantKey: "amopetcare",
//   // tenantKey: "dogspaw",
//   // tenantKey: "thehoundry",
//   // tenantKey: "pupncuts",
//   // tenantKey: "dogkingdom",
//   // tenantKey: "runloyalpetresort",
//   // tenantKey: "themuddymutt", 
//   // tenantKey: "fidofido-alpharetta",
//   applicationKey: "iTrust-PawOps",
//   version: "1.29",
// };


// export const environment = {             //prod
//   production: true,
//   basePath: "https://www.runloyal.com/may13",
//   userService: "https://www.runloyal.com/may13/user-service",
//   notificationService: "https://www.runloyal.com/may13/notification-service",
//   calendarService: "https://www.runloyal.com/may13/calendar-service",
//   messageService: "https://www.runloyal.com/may13/message-service",
//   paymentReturnUrl: "https://www.runloyal.com/suitepaws/thankyouKiosk.html",
//   // tenantKey: "dogsrus",
//   // tenantKey: "paw-palace",
//   // tenantKey: "pawops-demo",
//  // tenantKey: "suitepaws",
//   // tenantKey: "amopetcare",
//   // tenantKey: "thehoundry",
//   // tenantKey: "dogspaw",
//     // tenantKey: "pupncuts",
//   // tenantKey: "",
//   // tenantKey: "dogkingdom",
//   // tenantKey: "themuddymutt",
//   // tenantKey: "runloyalpetresort",
//   // tenantKey: "fidofido-alpharetta",
//   applicationKey: "iTrust-PawOps",
//   version: "1.0",
// };


// export const environment = {             //uat.pawops
//   production: false,
//   basePath: "https://uat.pawops.com/services",
//   userService: "https://uat.pawops.com/services/user-service",
//   notificationService: "https://uat.pawops.com/services/notification-service",
//   calendarService: "https://uat.pawops.com/services/calendar-service",
//   messageService: "https://uat.pawops.com/services/message-service",
//   paymentReturnUrl: "https://uat.pawops.com/suitepaws/thankyou.html",
//   // tenantKey: "dogsrus",
//     // tenantKey: "dogkingdom",
//   // tenantKey: "thehoundry",
//   // tenantKey: "paw-palace",
//   // tenantKey: "dogspaw",
//     // tenantKey: "themuddymutt",
//   // tenantKey: "pupncuts",
//   tenantKey: "suitepaws",
//   // tenantKey: "runloyalpetresort",
//   applicationKey: "iTrust-PawOps",
//   version: "1.29",
// };


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
