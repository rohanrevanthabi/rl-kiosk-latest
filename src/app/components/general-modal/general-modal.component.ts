import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from "@nativescript/angular";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-general-modal',
  templateUrl: './general-modal.component.html',
  styleUrls: ['./general-modal.component.css']
})
export class GeneralModalComponent implements OnInit {

  context : any;
  isDefaultPopup = true;

  constructor(
    private params: ModalDialogParams,
    private router: Router,      
  ) { 
    if (params.context.type && params.context.type == "default-popup") {
      this.isDefaultPopup = true;
      this.context = params.context;
    }
  }

  ngOnInit(): void {

  }

  defaultOkay() {
    this.params.closeCallback(true);
  }

  closeAndStay() {
    if(this.context && this.context.enableDiscard) {
      this.params.closeCallback(false);
    } else {
      this.params.closeCallback();
    }
  }

}
