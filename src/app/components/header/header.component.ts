import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { StorageHelper } from '../../services/storage.helper';
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  storageService = new StorageHelper();

  tenantLogo : any;

  @Input() isDisableHomeIcon :any;


  constructor(
    private modalService: ModalDialogService, 
    private router: Router,
    private viewContainerRef: ViewContainerRef,
  ) {
    this.storageService = new StorageHelper();
   }

  ngOnInit(): void {
    this.tenantLogo = this.storageService.getItem("tenantLogo");
  }

  goHome() {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "Confirm",
        cancelText : "Cancel",
        description : "Are you sure you want to go to home page?",
        header : "ALERT",
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(result) {
          setTimeout(() => {
            this.router.navigate(['flow-select']);
          }, 300);
        }
      });
  }

}
