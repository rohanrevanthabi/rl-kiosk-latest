import { Component, OnInit } from '@angular/core';
import { ModalDialogParams } from "@nativescript/angular";

@Component({
  selector: 'app-tip-popup',
  templateUrl: './tip-popup.component.html',
  styleUrls: ['./tip-popup.component.css']
})
export class TipPopupComponent implements OnInit {

  context : any;
  
  isTipPopup = false;
  isCustomTipPopup = false;

  
  tipAmount : any;
  tipInput : any;

  constructor(
    private params: ModalDialogParams,
  ) { 
    this.isTipPopup = true;
    this.context = params.context;
    this.tipAmount = this.context.tipAmount;
  }

  ngOnInit(): void {

  }

  tipOkay() {
    this.params.closeCallback(this.tipAmount);
  }

  tipChange(tip : any) {
    this.tipAmount = tip;
  }

  openCustomTip() {
    this.isTipPopup = false;
    this.isCustomTipPopup = true;
  }

  showTipPopup(bool : boolean) {
    if(bool) {
      this.tipAmount = this.tipInput;
      this.tipOkay();
    } else {
      this.isTipPopup = true;
      this.isCustomTipPopup = false;
    }
  }
  
  closeAndStay() {
    if(this.context && this.context.enableDiscard) {
      this.params.closeCallback(false);
    } else {
      this.params.closeCallback();
    }
  }

  isCustomTip() {
    if(!this.tipAmount || this.tipAmount == 15 || this.tipAmount == 18 || this.tipAmount == 20 || this.tipAmount == 25 || this.tipAmount == 0) {
      return false;
    }
    return true;
  }
}
