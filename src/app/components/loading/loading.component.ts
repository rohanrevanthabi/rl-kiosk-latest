import { Component, OnInit, Input } from '@angular/core';

import { registerElement } from '@nativescript/angular';
import { Gif } from 'nativescript-gif';
registerElement('Gif', () => Gif);

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  @Input() isLoading :any;
  @Input() showText :any;

  constructor() { }

  ngOnInit(): void {
    console.log("loading component");
  }

}
