import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'

import { LoginComponent } from './pages/login/login.component';
import { FlowSelectComponent } from './pages/flow-select/flow-select.component';
import { AppointmentsListComponent } from './pages/appointments-list/appointments-list.component';
import { AppointmentDetailComponent } from './pages/appointment-detail/appointment-detail.component';
import { UserEmailComponent } from './pages/user-email/user-email.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { PetBelongingsComponent } from './pages/pet-belongings/pet-belongings.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { InvoiceDetailComponent } from './pages/invoice-detail/invoice-detail.component';
import { CardsListComponent } from './pages/cards-list/cards-list.component';
import { ReviewComponent } from './pages/review/review.component';
import { QuestionariesComponent } from './pages/questionaries/questionaries.component';
import { AgreementsComponent } from './pages/agreements/agreements.component';
import { AgreementDetailComponent } from './pages/agreement-detail/agreement-detail.component';
import { MembershipListComponent } from './pages/membership-list/membership-list.component';
import { SelectPetComponent } from './pages/select-pet/select-pet.component';
import { MembershipSlotComponent } from './pages/membership-slot/membership-slot.component';
import { SelectLocationComponent } from './pages/select-location/select-location.component';
import { MembershipQuestionsComponent } from './pages/membership-questions/membership-questions.component';

export const routes: Routes = [
  {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full',
  },
  {
      path: 'login',
      component: LoginComponent,
  },
  {
      path: 'forgot-password',
      component: ForgotPasswordComponent,
  },
  {
    path: 'select-location',
    component: SelectLocationComponent,
  },
  {
      path: 'user-email',
      component: UserEmailComponent,
  },
  {
      path: 'flow-select',
      component: FlowSelectComponent,
  },
  {
      path: 'appointments-list',
      component: AppointmentsListComponent,
  },
  {
      path: 'appointment-detail',
      component: AppointmentDetailComponent,
  },
  {
      path: 'pet-belongings',
      component: PetBelongingsComponent,
  },
  {
    path: 'invoice-detail',
    component: InvoiceDetailComponent,
  },
  {
    path: 'cards',
    component: CardsListComponent,
  },
  {
    path: 'review',
    component: ReviewComponent,
  },
  {
    path: 'questionaries',
    component: QuestionariesComponent,
  },
  {
    path: 'agreements',
    component: AgreementsComponent,
  },
  {
    path: 'agreement-detail',
    component: AgreementDetailComponent,
  },
  {
    path: 'memberships',
    component: MembershipListComponent,
  },
  {
      path: 'membership-questions',
      component: MembershipQuestionsComponent,
  },
  {
    path: 'select-pet',
    component: SelectPetComponent,
  },
  {
    path: 'membership-slot',
    component: MembershipSlotComponent,
  },
  {
    path: 'payment',
    component: PaymentComponent,
  },
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule],
})
export class AppRoutingModule {}
