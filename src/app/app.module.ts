import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptModule } from '@nativescript/angular'

import { NativeScriptFormsModule, NativeScriptHttpClientModule } from '@nativescript/angular';
import { ModalDialogService } from "@nativescript/angular";
import { BarcodeScanner } from "@nstudio/nativescript-barcodescanner";
import { TNSCheckBoxModule } from '@nstudio/nativescript-checkbox/angular';

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
// import { ItemsComponent } from './item/items.component'
// import { ItemDetailComponent } from './item/item-detail.component'

import { LoginComponent } from './pages/login/login.component';
import { FlowSelectComponent } from './pages/flow-select/flow-select.component';
import { AppointmentsListComponent } from './pages/appointments-list/appointments-list.component';
import { AppointmentDetailComponent } from './pages/appointment-detail/appointment-detail.component';
import { UserEmailComponent } from './pages/user-email/user-email.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { PetBelongingsComponent } from './pages/pet-belongings/pet-belongings.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { InvoiceDetailComponent } from './pages/invoice-detail/invoice-detail.component';
import { CardsListComponent } from './pages/cards-list/cards-list.component';
import { ReviewComponent } from './pages/review/review.component';
import { QuestionariesComponent } from './pages/questionaries/questionaries.component';
import { AgreementsComponent } from './pages/agreements/agreements.component';
import { AgreementDetailComponent } from './pages/agreement-detail/agreement-detail.component';
import { MembershipListComponent } from './pages/membership-list/membership-list.component';
import { SelectPetComponent } from './pages/select-pet/select-pet.component';
import { MembershipSlotComponent } from './pages/membership-slot/membership-slot.component';
import { SelectLocationComponent } from './pages/select-location/select-location.component';
import { MembershipQuestionsComponent } from './pages/membership-questions/membership-questions.component';

import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoadingComponent } from './components/loading/loading.component';
import { TipPopupComponent } from './components/tip-popup/tip-popup.component';
import { GeneralModalComponent } from './components/general-modal/general-modal.component';

import { AlertService } from './services/alert.service';
import { UtilityService } from './services/utility.service';
import { AppService } from './services/app.service';


export function createBarcodeScanner() {
  return new BarcodeScanner();
}

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule, 
    AppRoutingModule,
    NativeScriptHttpClientModule,
    NativeScriptFormsModule,
    TNSCheckBoxModule,

  ],
  declarations: [
    AppComponent, 
    // ItemsComponent, 
    // ItemDetailComponent,
    LoginComponent,
    AppComponent,
    AppointmentsListComponent,
    AppointmentDetailComponent,
    UserEmailComponent,
    ForgotPasswordComponent,
    PetBelongingsComponent,
    CardsListComponent,
    PaymentComponent,
    FlowSelectComponent,
    InvoiceDetailComponent,
    ReviewComponent,
    QuestionariesComponent,
    AgreementsComponent,
    AgreementDetailComponent,
    MembershipListComponent,
    MembershipSlotComponent,
    SelectLocationComponent,
    SelectPetComponent,
    MembershipQuestionsComponent,
    HeaderComponent,
    FooterComponent,
    LoadingComponent,
    TipPopupComponent,
    GeneralModalComponent
  ],
  providers: [
    AlertService,
    UtilityService,
    AppService,
    ModalDialogService,
    { provide: BarcodeScanner, useFactory: createBarcodeScanner }
  ],
  schemas: [NO_ERRORS_SCHEMA],
  entryComponents: [
    TipPopupComponent,
    GeneralModalComponent
  ]
})
export class AppModule {}