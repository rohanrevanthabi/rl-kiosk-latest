import { Injectable } from '@angular/core';
import { TNSFancyAlert, TNSFancyAlertButton } from "@nstudio/nativescript-fancyalert";

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  showSuccess(title: string, subText: string, buttonText: string) {
    TNSFancyAlert.textDisplayOptions = {
      // titleFont: string,
      titleSize: 20,
      // bodyFont: string,
      bodySize: 18,
      // buttonFont: string,
      // buttonSize: number
    };    
    return TNSFancyAlert.showSuccess(
      title,
      subText,
      buttonText
    )
  }

  showError(title: string, subText: string, buttonText: string) {
    TNSFancyAlert.textDisplayOptions = {
      // titleFont: string,
      titleSize: 22,
      // bodyFont: string,
      bodySize: 19,
      // buttonFont: string,
      // buttonSize: number
    };   
    return TNSFancyAlert.showError(
      title,
      subText,
      buttonText
    )
  }

  showInfo(title: string, subText: string, buttonText: string) {
    TNSFancyAlert.textDisplayOptions = {
      // titleFont: string,
      titleSize: 22,
      // bodyFont: string,
      bodySize: 19,
      // buttonFont: string,
      // buttonSize: number
    };   
    return TNSFancyAlert.showInfo(
      title,
      subText,
      buttonText
    )
  }
}