import { knownFolders, File } from '@nativescript/core/file-system';
import { getString, setString, clear } from "@nativescript/core/application-settings";

export class StorageHelper {
  saveFile: File;

  constructor() {
    this.saveFile = knownFolders.documents().getFile('storage.json');
  }

  //not using this
  readItem(key: string | number) {
    const storageObj = this.saveFile.readTextSync();
    return storageObj ? (JSON.parse(storageObj))[key] : null;
  }

  writeItem(key: string | number, value: any) {
    console.log("this.saveFile.readTextSync()", this.saveFile.readTextSync());
    var storageObj = JSON.parse(this.saveFile.readTextSync());
    console.log(storageObj);
    storageObj[key] = value;
    this.saveFile.writeText(JSON.stringify(storageObj));
  }


  //only using this
  setItem(key: any, value: any) {
    setString(key, value);
  }

  getItem(key: any) {
    return getString(key) ? getString(key) : null;
  }

  clearData() {
    clear();
  }
}