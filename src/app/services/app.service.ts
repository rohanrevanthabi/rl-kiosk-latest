import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { throwError,  } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from "../../environment";
import { StorageHelper } from './storage.helper';
import * as geolocation from "@nativescript/geolocation";
import { CoreTypes } from '@nativescript/core';
import * as moment from 'moment-timezone';

import { device, screen, isAndroid, isIOS } from "@nativescript/core/platform";
// import { crashlytics } from "@nativescript/firebase/crashlytics";

var bghttp = require("@nativescript/background-http");
var session = bghttp.session("file-upload");

@Injectable({
    providedIn: 'root'
  })

export class AppService {

    storageService = new StorageHelper();

    userServiceUrl = `${environment.userService}`;
    calendarServiceUrl = `${environment.calendarService}`;
    messageServiceUrl = `${environment.messageService}`;
    notificationServiceUrl = `${environment.notificationService}`;
    baseUrl = `${environment.basePath}`;
    // tenantKey = `${environment.tenantKey}`;
    tenantKey = "";
    locationKey = "";
    timeZone = "";
    applicationKey = `${environment.applicationKey}`;
    versionCode = `${environment.version}`;
    paymentReturnUrl = `${environment.paymentReturnUrl}`;

    authObj : any;
    userId : any;
    accessToken : any;

    requestId = "x2slls33";             //temp

    facebookGraphAPIUrl = "https://graph.facebook.com/v7.0/";

    constructor(
        private http: HttpClient,
    ) { 
        this.storageService = new StorageHelper();
    }

    login(email: string, password: string) {
        let otpUrl = this.userServiceUrl + "/oauth/token";

        let myHeaders = new HttpHeaders({
            "Authorization": "Basic YXBwX3VzZXI6cGFzc3dvcmQ=",
            "Content-Type": "application/x-www-form-urlencoded",
         });
        const urlencoded = new FormData();
        urlencoded.append("grant_type", "password");
        urlencoded.append("username", email);
        urlencoded.append("password", password);
        // urlencoded.append("platform", 'WEB');
        urlencoded.append("platform", 'KIOSK');
        // urlencoded.append("lat", '');
        // urlencoded.append("lng", '');
        // urlencoded.append("source", 'KIOSK');
        // urlencoded.append("tenantKey", environment.tenantKey);
        urlencoded.append("organizationKey", "ORG_SUITEPAWS");
        console.log(urlencoded);

        return this.http.post(otpUrl, urlencoded, { headers: myHeaders })
    }

    forgotPassword(email : any) { 
        let otpUrl = this.userServiceUrl + "/forgot-password";
        let myHeaders = new HttpHeaders({
            "Authorization": "Basic YXBwX3VzZXI6cGFzc3dvcmQ=",
            "Content-Type": "application/json",
        });
        var body = {
            "userName": email,
            "tenantKey":this.tenantKey
        }

        return this.http.post(otpUrl, body, { headers: myHeaders })
    }

    setAuthVariables(tenant : any) {
        this.authObj = this.storageService.getItem('authObj');

        if(this.authObj) {
            this.authObj = JSON.parse(this.authObj);
            if(this.authObj["id"])
            this.userId = this.authObj["id"];
            //crashlytics.setUserId(this.userId);
            this.accessToken = this.authObj["access_token"];
            this.tenantKey = tenant;
        } else {
            console.log("no auth obj");
        }
    }

    setLocationVariables(location : any) {
        this.timeZone = location["timeZone"];
        this.storageService.setItem("timeZone", this.timeZone);
        this.locationKey = location["locationKey"];
        this.storageService.setItem("locationKey", this.locationKey);
        this.tenantKey = location["tenant"]["tenantKey"];
        moment.tz.setDefault(this.timeZone);
    }

    enableLocation() {
        console.log("calling in app service enable Location")
        if (isAndroid)
        {
            return geolocation.enableLocationRequest(true);
        }
        return geolocation.enableLocationRequest(false, true);
    }

    isLocationEnabled() {
        return geolocation.isEnabled();
    }
    
    getGeoLocation() {
        console.log('Getting location');
        return geolocation.getCurrentLocation({
          desiredAccuracy: CoreTypes.Accuracy.high,
          maximumAge: 5000,
          timeout: 20000
        });
    }

    getTenantData() {
        let locUrl = this.userServiceUrl + "/version/" + this.tenantKey + "/PORTAL/1.40";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.get(locUrl, {headers : headers})
    }

    getOrganizationData(org) {
        let locUrl = this.userServiceUrl + "/api/v3/" + org + "/users/" + this.userId;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.get(locUrl, {headers : headers})
    }

    getLocations(tenant) {
        let locUrl = this.userServiceUrl + "/kennel/" + tenant + "/locations";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.get(locUrl, {headers : headers})
    }

    getUserData(emailOrMobileNumber : any) {
        let locUrl = this.userServiceUrl + "/v3/search/" + this.tenantKey + "/autocomplete/userByEmailOrMobileno/" + encodeURIComponent(emailOrMobileNumber);
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    getUserPets(userKey : any) {
        let locUrl = this.userServiceUrl + "/pets/" + this.tenantKey + "/pets/" + userKey;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    getUserAppointments(emailOrMobileNumber : any, status : any) {
        let locUrl = this.userServiceUrl + "/admin/v1/" + this.tenantKey + "/appointments?emailOrMobileNumber=" + encodeURIComponent(emailOrMobileNumber) + "&status=" + status;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    getAppointmentObj(appointmentKey) {
        let locUrl = this.userServiceUrl + "/appointment/" + this.tenantKey + "/" + appointmentKey;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl)
        return this.http.get(locUrl, {headers : headers})
    }

    getUserMemberships(emailOrMobileNumber : any) {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + this.locationKey + "/" + this.userId + "/membershipSubscriptionsByEmailOrMobile?emailOrMobileNumber=" + encodeURIComponent(emailOrMobileNumber);
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    getAllMemberships() {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey  + "/" + this.locationKey + "/" + this.userId + "/memberships?sort=createdOn,desc&size=50&page=0";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    getMembershipData(membershipId : any) {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + this.locationKey + "/" + this.userId + "/memberships/" + membershipId;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    purchaseMembership(body : any, userKey : any) {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + this.locationKey + "/" + userKey + "/membershipSubscriptions";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.post(locUrl, body, {headers : headers})
    }

    getLodgingArea() {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + this.locationKey + "/lodgeByServiceBasedOn/MEMBERSHIP";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
        return this.http.get(locUrl, {headers : headers})
    }

    getEvents(units : any, startDate, endDate) {
        let locUrl = this.calendarServiceUrl + "/api/iTrust-PawOps/event/LODGE/" + units + "/Event?startDate=" + startDate + "&endDate=" + endDate;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
        return this.http.get(locUrl, {headers : headers})
    }

    createAppointmentForMembership(body, userKey) {
        let locUrl = this.userServiceUrl + "/api/v3/" + this.tenantKey + "/appointment/" + userKey;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.post(locUrl, body, {headers : headers})
    }

    updateAnswersForMembershipAppointment(body, userKey) {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + this.storageService.getItem("locationKey") + "/" + userKey + "/membershipSubscriptions/answers/";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.post(locUrl, body, {headers : headers})
    }

    getUserAgreements(locationKey : any) {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + locationKey + "/userAgreement/user/" + this.userId
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
        return this.http.get(locUrl, {headers : headers})
    }

    acceptAgreement(agreementBody, agreementId) {
        agreementBody["appointmentSource"] = isAndroid ? "ANDROID" : "IOS";

        let url;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        if(agreementId) {
            url = this.userServiceUrl + "/api/v2/"+ this.tenantKey +"/" + this.storageService.getItem("locationKey") + "/userAgreement/" + agreementId;
        } else {
            url = this.userServiceUrl + "/api/v2/"+ this.tenantKey +"/" + this.storageService.getItem("locationKey") + "/userAgreement";
        }
        return this.http.post(url, agreementBody, { headers: headers })
    }

    getReviewParameters() {
        let locUrl = this.userServiceUrl + "/review/" + this.tenantKey + "/parameter/" + this.storageService.getItem("locationKey") + "/APPOINTMENT";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.get(locUrl, {headers : headers})
    }

    reviewAppointment(body, reviewId) {
        let locUrl = this.userServiceUrl + "/review/" + this.tenantKey + "/" + reviewId;

        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
        return this.http.put(locUrl, body, { headers: headers })
    }

    updateAnswersInAppointment(body, appointmentKey) {
        let locUrl = this.userServiceUrl + "/api/v2/" + this.tenantKey + "/" + this.storageService.getItem("locationKey") + "/appointment/" + appointmentKey + "/answers";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         console.log(locUrl);
        return this.http.post(locUrl, body, {headers : headers})
    }
    
    getAppointmentInvoice(appointmentKey:any,locationKey:any) {
        let locUrl = this.userServiceUrl + "/api/v2/"+ this.tenantKey + "/" + locationKey+"/invoice/appointment/"+appointmentKey;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.get(locUrl, {headers : headers})
    }

    saveInvoice(appointmentKey:any,locationKey:any, request:any) {

        let locUrl = this.userServiceUrl + "/api/v2/"+ this.tenantKey + "/" + locationKey+"/invoice/appointment/"+appointmentKey;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
         
        return this.http.post(locUrl, request, {headers : headers})
      
    }

    getSavedCards() {
        let locUrl = this.baseUrl + "/payment-service/api/" + this.applicationKey + "/merchant/" + this.tenantKey + "/customer/" + this.userId + "/cards";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
        return this.http.get(locUrl, {headers : headers})
    }

    addCard(body) {
        let locUrl = this.baseUrl + "/payment-service/api/" + this.applicationKey + "/merchant/" + this.tenantKey + "/customer/" + this.userId + "/addCard";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });
        return this.http.post(locUrl, body, {headers : headers})
    }

    inviteUser(user) {
        var deviceType = "KIOSK";
        user["tenantKey"] = this.tenantKey;
        user["platform"] = deviceType;
        let locUrl = this.userServiceUrl + "/invite";
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
        });
        return this.http.put(locUrl, user, {headers : headers})
    }

    updateStatus(body : any, appointmentKey : string) {
        let url = this.userServiceUrl + "/admin/v1/" + this.tenantKey + "/appointments/" + appointmentKey + "/status";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.put(url, body, { headers: headers })
    }
    
    saveTip(body:any, estNo:any) {
        let url = this.baseUrl + "/payment-service/api/" + this.applicationKey + "/merchant/" + this.tenantKey + "/" + estNo + "/tip";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.post(url, body, { headers: headers })
        
    }

    getInvoice(body : any, estNo : any) {
        let url = this.baseUrl + "/payment-service/api/" + this.applicationKey + "/merchant/" + this.tenantKey + "/" + estNo + "/payment";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.post(url, body, { headers: headers })
    }

    getPaymentLink(body : any, estNo : any) {
        body['loggedInUser'] = {"user":{"userKey":this.userId}}
        let url = this.baseUrl + "/payment-service/api/" + this.applicationKey + "/merchant/" + this.tenantKey + "/" + estNo + "/payment";
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
         });

        return this.http.post(url, body, { headers: headers })
    }

    getSignatureImageUploadUrl(appointmentKey : any, image : any) {
        var extension = image.extension.split(".")[1];
        let locUrl = this.userServiceUrl + "/document/uploadUrl/?id=" + appointmentKey + "&uploadType=SIGNATURE&filename="+ image.name +"&mimeType=image/" + extension;
        let headers = new HttpHeaders({
            "authorization": "Bearer " + this.accessToken,
            "Content-Type": "application/json",
        });

        return this.http.get(locUrl, {headers : headers})
    }

    uploadImageToAws(url: any, image: any) {
        console.log("uploading to aws1");
        var extension = image.extension.split(".")[1];
        console.log("uploading to aws2", image.name + '.' + extension);
        return new Promise<String>((resolve, reject) => {
            console.log("uploading...");
            var request = {
                url: url,
                method: "PUT",
                headers: {
                    // "Content-Type": "application/octet-stream",
                    "File-Name": image.name,
                    "Content-Type": "image/" + extension,
                    "x-amz-acl": 'public-read'
                },
                description: "{ 'uploading': " + image.name + " }"
            };
    
            var task = session.uploadFile(image.path, request);
    
            task.on("progress", logEvent);
            task.on("error", logEvent);
            task.on("complete", logEvent);
    
            function logEvent(e : any) {
                console.log(e);
            }
        });
    }

}
