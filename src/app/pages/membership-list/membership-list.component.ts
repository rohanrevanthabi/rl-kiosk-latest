import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-membership-list',
  templateUrl: './membership-list.component.html',
  styleUrls: ['./membership-list.component.css']
})
export class MembershipListComponent implements OnInit {

  isLoading = false;

  userMemberships = [];
  userPets = [];
  allMemberships = [];
  user;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private modalService: ModalDialogService, 
    private router: Router,
    private viewContainerRef: ViewContainerRef,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      this.user = JSON.parse(params.user);
      this.getUserMemberships();
      this.getUserPets();
    });
  }

  getUserPets() {
    // this.isLoading = true;
    this.appService.getUserPets(this.user.userKey).subscribe(
      (res : any) => {
        console.log("getUserPets", res);
        this.userPets = res["list"] ? res["list"] : [];
        if(this.userPets.length == 0) {
          const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: false,
            context: {
                type : "default-popup",
                okText : "Okay",
                description : "You need atleast one pet to continue.",
                header : "No pets found",
                image:"~/assets/information.png"
            },
            ios : {
                height : 450,
                width : 400
            }
            };
            this.modalService.showModal(GeneralModalComponent, options)    
            .then((result: string) => {
                console.log("result", result);
                setTimeout(() => {
                  this.routerExtensions.back();
                }, 300);
            });
        }
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  getUserMemberships() {
    this.isLoading = true;
    this.appService.getUserMemberships(this.user.email).subscribe(
      (res : any) => {
        console.log("getUserMemberships", res);
        this.userMemberships = res["list"] ? res["list"] : [];
        // this.isLoading = false;
        this.getAllMemberships();
      },
      (error : any) => {
        this.getAllMemberships();
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  getAllMemberships() {
    this.appService.getAllMemberships().subscribe(
      (res : any) => {
        console.log("getAllMemberships", res);
        if(res["data"]) {
          this.allMemberships = res["data"]["content"] ? res["data"]["content"] : [];
        }
        this.isLoading = false;
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  selectMembership(membershipObj : any) {
    let navigationExtras: NavigationExtras = {
        queryParams: {
            "user" : JSON.stringify(this.user),
            "type" : 'check-in',
            "membership" : JSON.stringify(membershipObj.membership),
            "userPets" : JSON.stringify(this.userPets),
            "maxPetsAllowed" : membershipObj.membership.maxPetsAllowed
        }
    };
    this.router.navigate(['select-pet'], navigationExtras); 
  }

  buyMembership(membership : any) {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "Confirm",
        cancelText : "Cancel",
        description : "Are you sure you want to buy this membership?",
        header : membership.name,
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(result) {
          if(membership.restrictByPetCount) {
            let navigationExtras: NavigationExtras = {
                queryParams: {
                    "user" : JSON.stringify(this.user),
                    "type" : 'check-in',
                    "membership" : JSON.stringify(membership),
                    "userPets" : JSON.stringify(this.userPets),
                    "isMembershipPurchase" : "true",
                    "maxPetsAllowedOnPurchase" : membership.maxPetsAllowedOnPurchase
                }
            };
            setTimeout(() => {
              this.router.navigate(['select-pet'], navigationExtras); 
            }, 300);  
          } else {
            this.purchaseMembership(membership);
          }
        }
    });
  }
  
  purchaseMembership(membership : any) {
    this.isLoading = true;
    var body = {
        "membership": {
            "id": membership.id
        },
        "user": {
            "id": this.user.id
        },
        "pets": [
          
        ]
    }
    this.appService.purchaseMembership(body, this.user.userKey).subscribe(
      (res : any) => {
        // console.log(res);
        this.showSuccessPopup(membership);
        this.isLoading = false;
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  showSuccessPopup(membership) {
    let navigationExtras: NavigationExtras = {
        queryParams: {
            "user" : JSON.stringify(this.user),
            "type" : 'check-in',
            "membership" : JSON.stringify(membership),
            "userPets" : JSON.stringify(this.userPets)
        }
    };
    const options: ModalDialogOptions = {
    viewContainerRef: this.viewContainerRef,
    fullscreen: false,
    context: {
        type : "default-popup",
        okText : "Continue",
        description : "Membership purchased successfully.",
        header : "SUCCESS",
        image:"~/assets/confirm.png"
    },
    ios : {
        height : 450,
        width : 400
    }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
    .then((result: string) => {
        console.log("result", result);
        setTimeout(() => {
          this.router.navigate(['select-pet'], navigationExtras); 
        }, 300);
    });
  }
  
  getReadableDateShort(date) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('MMM DD, YYYY');
  }

  goToAgreement(agreement) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "agreement" : JSON.stringify([agreement])
      }
    };
    this.router.navigate(['agreement-detail'], navigationExtras);  
  }

  back() { 
    this.routerExtensions.back();
  }

}
