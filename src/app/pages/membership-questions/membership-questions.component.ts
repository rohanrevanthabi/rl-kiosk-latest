import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-membership-questions',
  templateUrl: './membership-questions.component.html',
  styleUrls: ['./membership-questions.component.css']
})
export class MembershipQuestionsComponent implements OnInit {

  isLoading = false;

  reservations = [];
  appointment;
  
  membership;
  user;
  pet;
  questions = [];
  answersBody = {};

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { }

  ngOnInit() {
    // this.route.queryParams.subscribe(params => {
    //   console.log(params);
      
    //   var appointmentObj = JSON.parse(params.appointment)[0];
    //   this.appointment = appointmentObj.appointmentDetails[0];
    //   this.reservations = this.appointment.reservationTypes;
    // });

    this.route.queryParams.subscribe(params => {
      console.log(params)
      this.membership = JSON.parse(params.membership);
      this.user = JSON.parse(params.user);
      this.pet = JSON.parse(params.pet);
      this.questions = this.membership.questions;
      console.log("ques", this.questions);

      if(this.questions.length == 0) {
        this.goToMembershipSlot();
      }
    });
  }

  goToMembershipSlot() {
    let navigationExtras: NavigationExtras = {
        queryParams: {
            "user" : JSON.stringify(this.user),
            "type" : 'check-in',
            "membership" : JSON.stringify(this.membership),
            "pet" : JSON.stringify(this.pet),
            "answersBody" : JSON.stringify(this.answersBody)
        }
    };
    this.router.navigate(['membership-slot'], navigationExtras);
  }

  updateAnswersInAppointment() {
    let answers = [];
    for(let j=0; j<this.questions.length; j++) {
      var body = {
        "appointmentDetail":{
          "id": ""
        },
        "answer": this.questions[j].answer,
        "question":{
          "id": this.questions[j].id
        }
      }
      if(!this.questions[j].deleted) {
        answers.push(body);
      }
    }
    this.answersBody = {
      "list" : answers
    }
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "OK",
        // cancelText : "Go back",
        description : "Your answers are saved.",
        header : "Thanks!",
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        setTimeout(() => {    
          this.goToMembershipSlot();
        }, 300);
      });
  }

  back() { 
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "Stay",
        cancelText : "Go back",
        description : "Your answers will be lost. Continue?",
        header : "Are you sure?",
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(!result) {
          setTimeout(() => {
            this.routerExtensions.back();
          }, 300);
        }
      });
  }

}
