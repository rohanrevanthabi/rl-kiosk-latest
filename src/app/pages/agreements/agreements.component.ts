import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";

@Component({
  selector: 'app-agreements',
  templateUrl: './agreements.component.html',
  styleUrls: ['./agreements.component.css']
})
export class AgreementsComponent implements OnInit {

  isLoading = false;

  agreements = [];
  appointment;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      this.appointment = JSON.parse(params.appointment)[0];
      this.getUserAgreements();
    });
  }

  getUserAgreements() {
    this.isLoading = true;
    this.appService.getUserAgreements(this.appointment.location.locationKey).subscribe(
      res => {
        this.isLoading = false;
        this.agreements = res["list"];
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }
  
  getReadableDateShort(date) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('MMM DD, YYYY');
  }

  goToAgreement(agreement) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "agreement" : JSON.stringify([agreement])
      }
    };
    this.router.navigate(['agreement-detail'], navigationExtras);  
  }

  back() { 
    this.routerExtensions.back();
  }

}
