import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";
import { StorageHelper } from '../../services/storage.helper';

@Component({
  selector: 'app-membership-slot',
  templateUrl: './membership-slot.component.html',
  styleUrls: ['./membership-slot.component.css']
})
export class MembershipSlotComponent implements OnInit {

  storageService = new StorageHelper();

  isLoading = false;
  continueText = ""

  units = [];
  events = [];
  membership;
  user;
  type;
  pet;
  answersBody;
  slotIndex = 0;
  isShowUnits = false;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { 
    this.storageService = new StorageHelper();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params)
      this.membership = JSON.parse(params.membership);
      this.user = JSON.parse(params.user);
      this.pet = JSON.parse(params.pet);
      this.answersBody = JSON.parse(params.answersBody);
      this.getLodgingUnits();
      this.continueText = "Checkin";
    });
  }

  getLodgingUnits() {
    this.isLoading = true;
    this.appService.getLodgingArea().subscribe(
      (res : any) => {
        // this.isLoading = false;
        if(res["data"]) {
          this.units = res["data"]["content"] ? res["data"]["content"] : [];
        }
        if(this.units.length > 0)
          this.getEvents();
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  getEvents() {
    this.isLoading = true;
    var unitsArray = [];
    for(let i=0; i<this.units.length; i++) {
      unitsArray.push(this.units[i].id);
    }
    var now = moment();
    this.appService.getEvents(unitsArray, now.startOf('year'), now.endOf('day')).subscribe(
      (res : any) => {
        // this.isLoading = false;
        this.events = res["list"] ? res["list"] : [];
        this.processSlots();
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  processSlots() {
    var events = this.events;
    for(let i=0; i<this.units.length; i++) {
      var usedArea = 0;
      for(let j=0; j<events.length; j++) {
        var array = events[j].participantVos;
        for(let k=0; k<array.length; k++) {
          if(array[k].resourceType == "LODGE") {
            if(array[k].resource == this.units[i].id) {
              usedArea++;
            }
          }
        }
      }
      this.units[i]["usedArea"] = usedArea;
    }
    this.isLoading = false;
    this.isShowUnits = true;
  }

  selectSlot(slot) {
    // let navigationExtras: NavigationExtras = {
    //     queryParams: {
    //         "user" : this.user,
    //         "pet" : this.pet,
    //         "type" : 'check-in',
    //         "membership" : JSON.stringify(this.membership)
    //     }
    //   };
    // this.router.navigate(['user-email'], navigationExtras);

    var now = moment();

    var body = {
      "location": {
          "locationKey": this.storageService.getItem("locationKey")
      },
      "appointmentDetails": [
          {
              "pet": {
                  "petKey": this.pet.petKey
              },
              "reservationTypes": [
                  {
                      "id": this.membership.reservationServices.reservationType.id,
                      "services": [
                          {
                              "id": this.membership.reservationServices.service.id,
                              "serviceAssignments": [
                                  {
                                      "date": now,
                                      "lodging": {
                                          "id": slot.id
                                      }
                                  }
                              ],
                              "addOn": false
                          }
                      ],
                      "name": "Membership",
                      "serviceBasedOn": "MEMBERSHIP",
                      "startDate": now,
                      "endDate": now.endOf('day')
                  }
              ]
          }
      ],
      "membershipId": this.membership.id,
      // "appointmentKey": "",
      "appointmentSource": "KIOSK",
      "appointmentDate": now,
      "appointmentEndTime": now.endOf('day')
     }

     this.isLoading = true;
     this.appService.createAppointmentForMembership(body, this.user.userKey).subscribe(
       (res : any) => {
        //  this.isLoading = false;
         this.saveQuestions(res["data"].id);
       },
       (error : any) => {
         console.log(error);
         this.isLoading = false;
       }
     );
  }

  saveQuestions(appointmentKey) {
    for(let i=0; i<this.answersBody.list.length; i++) {
      this.answersBody.list[i].appointmentDetail.id = appointmentKey;
    }
    this.appService.updateAnswersForMembershipAppointment(this.answersBody, this.user.userKey).subscribe(
      (res : any) => {
        this.isLoading = false;
        const options: ModalDialogOptions = {
          viewContainerRef: this.viewContainerRef,
          fullscreen: false,
          context: {
            type : "default-popup",
            okText : "OKAY",
            description : "Please proceed to front-desk.",
            header : this.pet.name + ' is now checked in!',
            image:"~/assets/checks.png"
          },
          ios : {
            height : 450,
            width : 400
          }
        };
        this.modalService.showModal(GeneralModalComponent, options)    
          .then((result: string) => {
            console.log("result", result);
            // if(result) {
              setTimeout(() => {
                this.router.navigate(['flow-select']);
              }, 300);
            // }
          });
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  // selectSlot(index) {
  //     this.slotIndex = index;
  // }


}
