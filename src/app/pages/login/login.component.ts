import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services/app.service';
import { StorageHelper } from '../../services/storage.helper';
import { UtilityService } from "../../services/utility.service";
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { device, screen, isAndroid, isIOS } from "@nativescript/core/platform";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  storageService = new StorageHelper();

  email = "uat@suitepaws.com";
  password = "a12345678";
  errorMsg = "";

  isLoading = false;


  location : any;
  constructor(
    public appService : AppService,
    private alertService : AlertService,
    private router: Router,
    private utilityService : UtilityService,
  ) { 
    this.storageService = new StorageHelper();

  }

  ngOnInit(): void {
    this.storageService.setItem("temp", "success2");
    console.log("yo...", this.storageService.getItem("temp"));

    // this.checkForLocationEnable();   


  }

  checkForLocationEnable() {
    this.appService.isLocationEnabled().then(
      (res : any) => {
        this.isLoading = false;
        console.log("checkForLocationEnable", res);
        if(res) {
          this.getUserLocation();
        } else {
          console.log("called enable Location");
          this.enableLocation();
        }

      },
      (error : any) => {
        console.log("checkForLocationEnable - error", error);
        this.isLoading = false;
      }
    );
  }

  enableLocation() {
    console.log("calling enable Location")
    this.appService.enableLocation().then(
      (res : any) => {
        console.log("enableLocation", res);
        this.getUserLocation();
      },
      (error : any) => {
        console.log("enableLocation - error", error);
        this.alertService.showError('Error', "Location permission is needed to continue.", 'Okay').then(() => {
          if (isAndroid)
          {
            this.enableLocation();
          }
        });
      }
    );
  }

  getUserLocation() {
    this.appService.getGeoLocation().then(
      (res : any) => {
        console.log("getUserLocation", res);
        this.location = res;
      },
      (error : any) => {
        console.log("getUserLocation - error", error);
        this.alertService.showError('Error', "Error getting user location.", 'Okay').then(() => {

        });
      }
    );
  }

  login() {
    if(this.email == "" || this.password == "") {
      this.errorMsg = "Fields cannot be empty.";
      return;
    }
    console.log("valid email",this.utilityService.isValidEmail(this.email));
    if(!this.utilityService.isValidEmail(this.email)) {
      this.errorMsg = "Please enter a valid email.";
      return;
    }
    // if (!this.location) {
    //   this.errorMsg = "Please allow your current location to login";
    //   return;
    // }

    this.isLoading = true;
    this.errorMsg = "";
    // this.appService.login(this.email, this.password,this.location.latitude,this.location.longitude).subscribe(
    this.appService.login(this.email, this.password).subscribe(
      (res : any) => {
        this.isLoading = false;
        console.log("loggedin");
        console.log(res["tenant"]);
        if(res["userType"] == 'ADMIN' || res["userType"] == 'EMPLOYEE') {
          this.alertService.showSuccess('Success', 'Login successful.', 'Next').then(() => {
            this.storageService.setItem("authObj", JSON.stringify(res));
            this.appService.setAuthVariables(res["tenant"]);
            this.getOrgData(res["organization"]);
          });
        } else {
          this.alertService.showError('Error', 'Logged in user is not an admin.', 'Okay').then(() => {
            this.email == "";
            this.password == "";
          });
        }
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
        var errorMsg = error.error ? error.error.message ? error.error.message : "Unknown error." : "Unknown error.";
        this.alertService.showError('Error', errorMsg, 'Okay').then(() => {
          this.email == "";
          this.password == "";
        });
      }
    );
  }

  getOrgData(org) {
    this.isLoading = true;
    this.appService.getOrganizationData(org).subscribe(
      (res : any) => {
        this.isLoading = false;
        if(res["data"]) {
          var locations = res["data"]["assignedLocations"];
          console.log(locations);
          if(locations) {
            if(locations.length == 0) {
              this.noLocationError();
            } 
            else if(locations.length == 1) {
              this.getLocations(locations[0].tenantKey);
            }
            else if(locations.length > 1) {
              let navigationExtras: NavigationExtras = {
                queryParams: {
                  "locations" : JSON.stringify(locations)
                }
              };
              this.router.navigate(['select-location'], navigationExtras);
            }
          } else {
            this.noLocationError();
          }
        } else {
          this.noLocationError();
        }
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  noLocationError() {
    this.alertService.showError('Error', 'No location assigned to this user.', 'Okay').then(() => {
      this.email == "";
      this.password == "";
    });
  }

  getLocations(tenantKey) {
    this.isLoading = true;
    this.appService.getLocations(tenantKey).subscribe(
      (res : any) => {
        // console.log(res);
        var locations = res["list"] ? res["list"] : [];
        if(locations.length > 0) {
          this.appService.setLocationVariables(locations[0]);
          this.router.navigate(['flow-select']);
        } else {
          this.noLocationError();
        }
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  goToForgotPass() {
    this.router.navigate(['forgot-password']);
  }

}
