import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-select-pet',
  templateUrl: './select-pet.component.html',
  styleUrls: ['./select-pet.component.css']
})
export class SelectPetComponent implements OnInit {

  isLoading = false;
  continueText = ""

  membership;
  user;
  userPets = [];
  isMembershipPurchase;
  maxPetsAllowedOnPurchase;
  type;
  petIndex = 0;
  selectedPetCount = 0;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      this.membership = JSON.parse(params.membership);
      this.user = JSON.parse(params.user);
      this.userPets = JSON.parse(params.userPets);
      this.isMembershipPurchase = params.isMembershipPurchase;
      this.maxPetsAllowedOnPurchase = params.maxPetsAllowedOnPurchase;
      this.type = params.type;
      console.log(this.maxPetsAllowedOnPurchase);

      this.continueText = "Continue";
    });
  }

  continue() {
    if(this.isMembershipPurchase == "true") {
      // console.log(this.selectedPetCount);
      // console.log(this.maxPetsAllowedOnPurchase);
      this.purchaseMembership();
    } else {
      if(this.membership.questions && this.membership.questions.length > 0) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
              "user" : JSON.stringify(this.user),
              "membership" : JSON.stringify(this.membership),
              "pet" : JSON.stringify(this.userPets[this.petIndex])
          }
        };
        this.router.navigate(['membership-questions'], navigationExtras); 
      } else {
        this.goToMembershipSlot();
      }
    }
  }

  goToMembershipSlot() {
    let navigationExtras: NavigationExtras = {
        queryParams: {
            "user" : JSON.stringify(this.user),
            "type" : 'check-in',
            "membership" : JSON.stringify(this.membership),
            "pet" : JSON.stringify(this.userPets[this.petIndex]),
            "answersBody" : JSON.stringify({})
        }
    };
    this.router.navigate(['membership-slot'], navigationExtras);
  }

  purchaseMembership() {
    var membership = this.membership;
    this.isLoading = true;
    var body = {
        "membership": {
            "id": membership.id
        },
        "user": {
            "id": this.user.id
        },
        "pets": [
          {
            "id" : this.userPets[this.petIndex].id
          }
        ]
    }
    this.appService.purchaseMembership(body, this.user.userKey).subscribe(
      (res : any) => {
        // console.log(res);
        this.showSuccessPopup();
        this.isLoading = false;
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  showSuccessPopup() {
    const options: ModalDialogOptions = {
    viewContainerRef: this.viewContainerRef,
    fullscreen: false,
    context: {
        type : "default-popup",
        okText : "Continue",
        description : "Membership purchased successfully.",
        header : "SUCCESS",
        image:"~/assets/confirm.png"
    },
    ios : {
        height : 450,
        width : 400
    }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
    .then((result: string) => {
        console.log("result", result);
        setTimeout(() => {
          let navigationExtras: NavigationExtras = {
            queryParams: {
              "user" : JSON.stringify(this.user),
            }
          };
          this.router.navigate(['memberships'], navigationExtras);   
        }, 300);
    });
  }

  isPetActive(index) {
    return this.userPets[index]["isSelected"];
  }

  selectPet(index) {
    this.userPets[index]["isSelected"] = !this.userPets[index]["isSelected"];
    if(this.userPets[index]["isSelected"]) {
      this.selectedPetCount++;
    } else {
      this.selectedPetCount--;
    }





      // this.petIndex = index;
      // this.continueText = "Checkin " + this.userPets[0].name;
  }


}
