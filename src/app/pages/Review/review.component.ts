import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

// import { registerElement } from '@nativescript/angular';
// import { StarRating } from 'nativescript-star-ratings';
// registerElement('StarRating', () => StarRating);

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.css']
})
export class ReviewComponent implements OnInit {

  isLoading = false;
  shareTypes = ["Display my name", "First name only", "Anonymously"];
  selectedShareType;
  tenantName;

  ratingValue = 0;
  max = 5;

  efficiency = 0;
  recommend = 0;
  useAgain = 0;

  parameters = [];

  reviewNotes;

  reviewId;

  isDisplayName = true;
  isShareReview = true;

  appointment;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    // private changeDetectorRef: ChangeDetectorRef,
    private modalService: ModalDialogService, 
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.appointment = JSON.parse(params.appointment)[0];
      this.getAppointmentObj();
      // this.reviewId = 415;
    });
    this.getReviewParameters();
  }

  getAppointmentObj() {
    this.isLoading = true;
    this.appService.getAppointmentObj(this.appointment.appointmentKey).subscribe(
      res => {
        console.log(res);
        var appt = res["data"];
        this.isLoading = false;
        this.reviewId = appt.review.id;
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  getReviewParameters() {
    this.isLoading = true;
    this.appService.getReviewParameters().subscribe(
      res => {
        console.log(res);
        this.parameters = res["list"];
        this.isLoading = false;
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  sliderChange(event, parameter) {
    parameter.value = event.value;
  }

  ratingChange(event) {
    this.ratingValue = event.value;
  }

  submit() {
    if(this.ratingValue == 0) {
      const options: ModalDialogOptions = {
        viewContainerRef: this.viewContainerRef,
        fullscreen: false,
        context: {
          type : "default-popup",
          okText : "OK",
          cancelText : "Close",
          description : "Please select a star before submitting.",
          header : "INFO"
        }
      };
      this.modalService.showModal(GeneralModalComponent, options)    
        .then((result: string) => {
          console.log("result", result);
        });
      return;
    }
    if(!this.reviewNotes || this.reviewNotes == "") {
      const options: ModalDialogOptions = {
        viewContainerRef: this.viewContainerRef,
        fullscreen: false,
        context: {
          type : "default-popup",
          okText : "OK",
          cancelText : "Close",
          description : "Please write a review before submitting.",
          header : "INFO"
        }
      };
      this.modalService.showModal(GeneralModalComponent, options)    
        .then((result: string) => {
          console.log("result", result);
        });
      return;
    }

    var body = {
      // displayPetOwner : this.isDisplayName,
      displayReviewNameType : this.selectedShareType ? this.selectedShareType : "DISPLAY_MY_NAME",
      shared : this.isShareReview,
      reviewText:this.reviewNotes,
      finalRating: Number(this.ratingValue),
      parameters:[]
    };

    for(let i=0; i<this.parameters.length; i++) {
      if(!this.parameters[i].value) {
        this.parameters[i].value = 0;
      }
      var paramObj = {
        parameterId : this.parameters[i].parameterId,
        rating : this.parameters[i].value
      }
      body.parameters.push(paramObj);
    } 
    console.log(body);

    this.isLoading = true;
    this.appService.reviewAppointment(body, this.reviewId).subscribe(
      res => {
        this.isLoading = false;
        console.log(res);
        const options: ModalDialogOptions = {
          viewContainerRef: this.viewContainerRef,
          fullscreen: false,
          context: {
            type : "default-popup",
            okText : "Ok",
            cancelText : "Close",
            description : "Thank you for sharing your feedback!",
            header : "SUCCESS"
          }
        };
        this.modalService.showModal(GeneralModalComponent, options)    
          .then((result: string) => {
            console.log("result", result);
            setTimeout(() => {
              this.router.navigate(['flow-select']);
            }, 300);
          });
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  changeCheckedRadio(type) {
    if(type == 'name') {
      this.isDisplayName = !this.isDisplayName; 
    }
    if(type == 'share') {
      this.isShareReview = !this.isShareReview; 
    }
  }

  onSelectedIndexChanged(args) {
    const picker = args.object;
    var index = picker.selectedIndex
    console.log(`index: ${picker.selectedIndex}; item" ${this.shareTypes[picker.selectedIndex]}`);
    if(index == 0) {
      this.selectedShareType = "DISPLAY_MY_NAME";
    }
    if(index == 1) {
      this.selectedShareType = "DISPLAY_FIRTNAME_ONLY";
    }
    if(index == 2) {
      this.selectedShareType = "ANONYMOUSLY";
    }
  }

  back() {
    this.routerExtensions.back();
  }

}
