import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-questionaries',
  templateUrl: './questionaries.component.html',
  styleUrls: ['./questionaries.component.css']
})
export class QuestionariesComponent implements OnInit {

  isLoading = false;

  reservations = [];
  appointment;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      var appointmentObj = JSON.parse(params.appointment)[0];
      this.appointment = appointmentObj.appointmentDetails[0];
      this.reservations = this.appointment.reservationTypes;
    });
  }

  updateAnswersInAppointment() {
    let answers = [];
    for(let i=0; i<this.reservations.length; i++) {
      for(let j=0; j<this.reservations[i].questions.length; j++) {
        var body = {
          "appointmentDetail":{
            "id": this.appointment.id
          },
          "answer": this.reservations[i].questions[j].answer,
          "question":{
            "id": this.reservations[i].questions[j].id
          }
        }
        if(!this.reservations[i].questions[j].deleted) {
          answers.push(body);
        }
      }
    }
    var answersBody = {
      "list" : answers
    }
    this.isLoading = true;
    this.appService.updateAnswersInAppointment(answersBody, this.appointment.appointmentId).subscribe(
      res => {
        console.log(res);
        this.isLoading = false;
        const options: ModalDialogOptions = {
          viewContainerRef: this.viewContainerRef,
          fullscreen: false,
          context: {
            type : "default-popup",
            okText : "OK",
            // cancelText : "Go back",
            description : "Your answers are saved.",
            header : "Thanks!",
            image:"~/assets/information.png"
          },
          ios : {
            height : 450,
            width : 400
          }
        };
        this.modalService.showModal(GeneralModalComponent, options)    
          .then((result: string) => {
            console.log("result", result);
            setTimeout(() => {
              this.routerExtensions.back();
            }, 300);
          });
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  back() { 
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "Stay",
        cancelText : "Go back",
        description : "Your answers will be lost. Continue?",
        header : "Are you sure?",
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(!result) {
          setTimeout(() => {
            this.routerExtensions.back();
          }, 300);
        }
      });
  }

}
