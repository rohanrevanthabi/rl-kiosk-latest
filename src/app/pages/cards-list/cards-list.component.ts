import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AppService } from '../../services/app.service';
import { AlertService } from '../../services/alert.service';
import { EventData, TextView } from '@nativescript/core'
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";
// import { Page } from "tns-core-modules/ui/page";

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.component.html',
  styleUrls: ['./cards-list.component.css']
})
export class CardsListComponent implements OnInit {

  type : any;
  appointment : any;
  invoiceDetails;
  routeAfterAddingCard;

  cards = [];
  isLoading = false;

  selectedCardIndex;

  constructor(
    private router: Router,
    public appService : AppService,
    private alertService : AlertService,
    private routerExtensions: RouterExtensions,
    private route: ActivatedRoute,
    // private page:Page,
    // private _ngZone: NgZone,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 

  ) { 
    // this.page.on('loaded', () => {
    //   console.log("page loaded!");
    //   this._ngZone.run(() => {
    //     this.getSavedCards();
    //   })
    // });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.type = params.type;
      this.appointment = JSON.parse(params.appointment);
      this.cards = this.appointment.cards;
      this.invoiceDetails = this.appointment.invoiceDetails;
      this.selectedCardIndex = 0;
    });
    console.log(this.cards);
    console.log(this.appointment.user)
  }

  getSavedCards() {
    this.isLoading = true;
    this.appService.getSavedCards().subscribe(
      res => {
        this.isLoading = false;
        this.cards = res["list"];
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  selectCard(index) {
    console.log(index);
    this.selectedCardIndex = index;
  }

  getPaymentLink(isSavedCard) {
    this.isLoading = true;

    // if(this.routeAfterAddingCard) {
    //   this.addNewCard();
    //   return;
    // }
    var body = { 
      "paymentType": "Credit", 
      "transactionType": "Sale", 
      "applicationReturnUrl": this.appService.paymentReturnUrl, 
      "amount": this.invoiceDetails.balanceDueToDisplay,
      "paymentMode": "CARD_NOT_PRESENT",
      "appSource":"KIOSK",
      "loggedInUser":{
        "userKey": this.appointment.user.userKey
      }
    }
    if(isSavedCard) {
      body["merchantTokenId"] = this.cards[this.selectedCardIndex].merchantTokenId;
    }

    this.appService.getPaymentLink(body, this.appointment.estimateNumber).subscribe(
      res => {
        this.isLoading = false;
        var paymentDetails = res["data"];

        if(!isSavedCard) {
          let navigationExtras: NavigationExtras = {
            queryParams: {
              "type" : this.type,
              "appointment" : JSON.stringify(this.appointment),
              "paymentUrl" : paymentDetails.checkoutUrl
            }
          };
          this.router.navigate(['payment'], navigationExtras);
          // this.setupWebViewInterface(paymentDetails.checkoutUrl);
        } else {
          if(paymentDetails["paymentStatus"] == "FAILURE" || paymentDetails["paymentStatus"] == "CANCELLED") {
            this.showPaymentRespPopup("MESSAGE", "Your transaction cancelled, Please try again after sometime.", true);
          } else {
            this.showPaymentRespPopup("SUCCESS", "Thank you for your payment.", false);
          }
        }
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  addNewCard() {
    var body = {
      "applicationReturnUrl" : this.appService.paymentReturnUrl,
      "invoiceVo" : {
        "customerVO" : {
          "userKey": this.appointment.user.userKey,
          "firstName":this.appointment.user.firstName,
          "lastName":this.appointment.user.lastName,
          "email":this.appointment.user.email
        }
      }
    }
    this.appService.addCard(body).subscribe(
      res => {
        this.isLoading = false;
        var paymentDetails = res["data"];
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "type" : this.type,
            "appointment" : JSON.stringify(this.appointment),
            "paymentUrl" : paymentDetails.addCardUrl,
            "routeAfterAddingCard" : true
          }
        };
        this.router.navigate(['payment'], navigationExtras);
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  showPaymentRespPopup(title, description, isStayInSamePage) {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "OKAY",
        description : description,
        header : title,
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(!isStayInSamePage) {
          setTimeout(() => {
            this.updateCheckoutAppointment();
          }, 300);
        }
      });
  }

  updateCheckoutAppointment() {
    var request = {'status':'CHECKOUT','belongingsNotes':this.appointment.belongingsNotes,'reviewRequired':true};
        console.log(request);
        this.isLoading = true;
        this.appService.updateStatus(request, this.appointment.appointmentKey)
        .subscribe(
          (res : any) => {
            this.isLoading = false;
            console.log(res);
            this.alertService.showSuccess(this.appointment.appointmentDetails[0].pet.name + ' is now checked out!','Please proceed to front-desk.' , 'OK').then(() => {
              // this.router.navigate(['flow-select']);
              let navigationExtras: NavigationExtras = {
                queryParams: {
                  "appointment" : JSON.stringify(this.appointment),
                }
              };
              this.router.navigate(['review'], navigationExtras);
            });
          },
          (error : any) => {
            this.isLoading = false;
            console.log("error", error);
          }
        );
  }

  goTOInvoice() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify(this.appointment),
      }
    };
    this.router.navigate(['invoice-detail'], navigationExtras);
  }
  
  addNotesAndBelongings() {


  }

  goBack() {
    this.routerExtensions.back();
    // this.router.navigate(['flow-select']);
  }


}
