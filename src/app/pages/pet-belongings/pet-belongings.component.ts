import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AppService } from '../../services/app.service';
import { AlertService } from '../../services/alert.service';
import { EventData, TextView } from '@nativescript/core'
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-pet-belongings',
  templateUrl: './pet-belongings.component.html',
  styleUrls: ['./pet-belongings.component.css']
})
export class PetBelongingsComponent implements OnInit {

  type : any;
  appointment : any;

  petBelongings : any;
  notes : any;

  isLoading = false;

  constructor(
    private router: Router,
    public appService : AppService,
    private alertService : AlertService,
    private routerExtensions: RouterExtensions,
    private route: ActivatedRoute,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 

  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.type = params.type;
      this.appointment = JSON.parse(params.appointment);
    });
   
  }

  onTextChange(args: EventData) {
    const tv = args.object as TextView
    console.log(tv.text)
    this.petBelongings = tv.text;
  }

  goTOInvoice() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify(this.appointment),
      }
    };
    this.router.navigate(['invoice-detail'], navigationExtras);
  }
  
  addNotesAndBelongings() {
    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     "type" : this.type,
    //     "appointment" : JSON.stringify(this.appointment),
    //     "belongingsNotes" : this.petBelongings,
    //     "notes" : this.notes
    //   }
    // };
    this.isLoading = true;
    var request = {'status':'CHECKIN','belongingsNotes':this.petBelongings,'notes':[{"description":this.notes}],"checkinSource":"KIOSK"};
    console.log(request);
    this.appService.updateStatus(request, this.appointment.appointmentKey)
    .subscribe(
      (res : any) => {
        console.log(res);
        this.isLoading = false;
        const options: ModalDialogOptions = {
          viewContainerRef: this.viewContainerRef,
          fullscreen: false,
          context: {
            type : "default-popup",
            okText : "OKAY",
            description : "Please proceed to front-desk.",
            header : this.appointment.appointmentDetails[0].pet.name + ' is now checked in!',
            image:"~/assets/checks.png"
          },
          ios : {
            height : 450,
            width : 400
          }
        };
        this.modalService.showModal(GeneralModalComponent, options)    
          .then((result: string) => {
            console.log("result", result);
            // if(result) {
              setTimeout(() => {
                this.router.navigate(['flow-select']);
              }, 300);
            // }
          });
      },
      (error : any) => {
        this.isLoading = false;
        console.log("error", error);
      }
    );
  }

  goBack() {
    this.routerExtensions.back();
    // this.router.navigate(['flow-select']);
  }


}
