import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-agreement-detail',
  templateUrl: './agreement-detail.component.html',
  styleUrls: ['./agreement-detail.component.css']
})
export class AgreementDetailComponent implements OnInit {

  isLoading = false;

  agreement;
  signature;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private modalService: ModalDialogService, 
    private activatedRoute: ActivatedRoute, 
    private viewContainerRef: ViewContainerRef,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      this.agreement = JSON.parse(params.agreement)[0];
    });
  }


  acceptAgreement() {
    if(!this.signature || this.signature == " ") {
      return;
    }

    var body = {
      "user":{
        "id": this.agreement.user.id
      },
      "agreement":{
        "id":this.agreement.agreement.id
      }, 
      "userAgreementStatus": "SIGNED",
      "signedName": this.signature,
      "signedDate": moment().valueOf(),
      "subject": this.agreement.subject,
      "subjectKey": this.agreement.subjectKey
    }

    // alert("this.agreement.id:" + this.agreement.id);
    // alert("body:" + JSON.stringify(body));
    // if(!this.agreement.id) {
    //   alert(this.agreement);
    // }

    this.isLoading = true;
    this.appService.acceptAgreement(body, this.agreement.id).subscribe(
      res => {
        // alert(JSON.stringify(res));
        this.isLoading = false;
        this.showSuccessPopup();
      },
      error => {
        // alert(JSON.stringify(error));
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  showSuccessPopup() {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "OK",
        cancelText : "Close",
        description : "Thanks for accepting the agreement.",
        header : "Thank You"
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        // setTimeout(() => {
        //   this.back();          
        // }, 300);
      });
  }

}
