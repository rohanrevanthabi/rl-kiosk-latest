import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";
import { StorageHelper } from '../../services/storage.helper';


@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.css']
})
export class AppointmentDetailComponent implements OnInit {

  storageService = new StorageHelper();

  appointmentObj : any;
  type : any;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private modalService: ModalDialogService, 
    private viewContainerRef: ViewContainerRef,

  ) {
    this.storageService = new StorageHelper();
   }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      this.type = params.type;
      this.appointmentObj = JSON.parse(params.appointment)[0];
      this.storageService.setItem("locationKey", this.appointmentObj.location.locationKey);
    });
    console.log("appointment page open");
  }

  goBack() {
    // this.router.navigate(['user-email']);
    this.routerExtensions.back();
  }

  goNext() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify(this.appointmentObj)
      }
    };
    if (this.type == 'check-in'){
      this.router.navigate(['invoice-detail'], navigationExtras);
    }
    else{
      if (this.appointmentObj.belongingsNotes && this.appointmentObj.belongingsNotes.length > 0)
      {
        this.router.navigate(['pet-belongings'], navigationExtras);
      }else{
        this.router.navigate(['invoice-detail'], navigationExtras);
      }
    }

  }

  getReadableDate(date:any) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('dddd MMMM Do YYYY h:mm A');
    // return new Date(date);
  } 

  getReadableDateShort(date:any) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('ddd Do MMM YYYY');
  }

  isShowAddServices(services:any) {
    if(!services) {
      return false;
    }
    var addServices = services.filter((obj:any) => obj.isAdditional);
    return addServices.length > 0;
  }


  isShowImmunizations(reservationTypes:any) {
    let immunizationAvailable = false;
    for(let i=0; i<reservationTypes.length; i++) {
      if(reservationTypes[i].immunizationTypeId) {
        immunizationAvailable = true;
      }
    }
    return immunizationAvailable;
  }

  isShowFoodAndFeeding(reservationTypes:any) {
    let foodNFeedingRequired = false;
    for(let i=0; i<reservationTypes.length; i++) {
      if(reservationTypes[i].feedingRequired) {
        foodNFeedingRequired = true;
      }
    }
    return foodNFeedingRequired;
  }

  isShowMedication(reservationTypes:any) {
    let medicationRequired = false;
    for(let i=0; i<reservationTypes.length; i++) {
      if(reservationTypes[i].medicationRequired) {
        medicationRequired = true;
      }
    }
    return medicationRequired;
  }

  isShowAgreements(appointment:any) {
    return appointment.agreementRequired;
  }

  isShowQuestionnaire(appointment : any) {
    var showQuestions = false;
    for(let i=0; i<appointment.appointmentDetails.length; i++) {
      var questions = [];
      for(let j=0; j<appointment.appointmentDetails[i].reservationTypes.length; j++) {
        if(appointment.appointmentDetails[i].reservationTypes[j].questions) {
          for(let k=0; k<appointment.appointmentDetails[i].reservationTypes[j].questions.length; k++) {
            if(appointment.appointmentDetails[i].reservationTypes[j].questions[k].deleted == false) {
              questions.push(appointment.appointmentDetails[i].reservationTypes[j].questions[k]);
            }
          }
        }
      }
      var answeredQuestions = [];
      if(appointment.appointmentDetails[i].answers) {
        answeredQuestions = appointment.appointmentDetails[i].answers.filter(question => question.answer);
      }
      // if(answeredQuestions.length != questions.length) {
      //   showQuestions = true;
      // }
      if(answeredQuestions.length == 0) {
        showQuestions = true;
      }
    }
    return showQuestions;
  }

  goToImmunizations(appointment:any) {
    this.showPopup('Immunization Required or Expired', "Please go to front-desk with your immunization records");
  }
  goToFoodAndFeeding(appointment:any) {
    this.showPopup('Feeding Info Required', "Please go to front-desk with your feeding infos");
  }
  goToMedication(appointment:any) {
    this.showPopup('Medication Info Required', "Please go to front-desk with your medication");
  }

  goToAgreements(appointment:any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify([appointment])
      }
    };
    this.router.navigate(['agreements'], navigationExtras);   
  }
  
  goToQuestionnaire(appointment:any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify([appointment])
      }
    };
    this.router.navigate(['questionaries'], navigationExtras);   
  }



  showPopup(title: String, description:String) {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "OKAY",
        description : description,
        header : title,
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(result) {
        }
      });
  }
}
