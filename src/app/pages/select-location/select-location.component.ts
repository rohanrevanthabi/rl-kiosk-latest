import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
// import * as moment from 'moment';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';
import { RouterExtensions } from "@nativescript/angular";
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

@Component({
  selector: 'app-select-location',
  templateUrl: './select-location.component.html',
  styleUrls: ['./select-location.component.css']
})
export class SelectLocationComponent implements OnInit {

  isLoading = false;

  locations = [];
  locationIndex = 0;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private routerExtensions: RouterExtensions,
    private alertService : AlertService,
    private router: Router,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.locations = JSON.parse(params.locations);
    });
  }  

  getLocations() {
    var tenant = this.locations[this.locationIndex].tenantKey;
    this.isLoading = true;
    this.appService.getLocations(tenant).subscribe(
      (res : any) => {
        // console.log(res);
        var locations = res["list"] ? res["list"] : [];
        if(locations.length > 0) {
          this.appService.setLocationVariables(locations[0]);
          this.router.navigate(['flow-select']);
        } else {
          this.noLocationError();
        }
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }
  
  noLocationError() {
    this.alertService.showError('Error', 'No location assigned to this user.', 'Okay').then(() => {
      // setTimeout(() => {
      //   this.router.navigate(['login']);      
      //   }, 300);
    });
  }

  selectLocation(index) {
      this.locationIndex = index;
    //   this.continueText = "Checkin " + this.pets[0].name;
  }


}
