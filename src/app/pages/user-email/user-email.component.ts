import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AppService } from '../../services/app.service';
import { AlertService } from '../../services/alert.service';
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";
import { BarcodeScanner } from "@nstudio/nativescript-barcodescanner";

@Component({
  selector: 'app-user-email',
  templateUrl: './user-email.component.html',
  styleUrls: ['./user-email.component.css']
})
export class UserEmailComponent implements OnInit {

  constructor(
    private router: Router,
    public appService : AppService,
    private alertService : AlertService,
    private route: ActivatedRoute,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { }

  // name : string = "demo@demo.com";
  // name : string = "uat@suitepaws.com"
  // name : string = "rohan@runloyal.com";
  // name = "anand+uat21@runloyal.com";
  name = "anandbabupnr@gmail.com";
  type : any;

  isLoading = false;

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.type = params.type;
    });
  }

  onReturnPress(event: any) {

  }

  onFocus(event: any) {

  }

  onBlur(event: any) {

  }

  scanQRCode() {
    let barcodescanner = new BarcodeScanner();

    barcodescanner.scan({
      formats: "QR_CODE",
      cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
      cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
      // message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
      showFlipCameraButton: true,   // default false
      preferFrontCamera: true,     // default false
      showTorchButton: true,        // default false
      beepOnScan: true,             // Play or Suppress beep on scan (default true)
      fullScreen: true,             // Currently only used on iOS; with iOS 13 modals are no longer shown fullScreen by default, which may be actually preferred. But to use the old fullScreen appearance, set this to 'true'. Default 'false'.
      torchOn: false,               // launch with the flashlight on (default false)
      closeCallback: () => { console.log("Scanner closed")}, // invoked when the scanner was closed (success or abort)
      resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
      // orientation: orientation,     // Android only, default undefined (sensor-driven orientation), other options: portrait|landscape
      openSettingsIfPermissionWasPreviouslyDenied: true, // On iOS you can send the user to the settings app if access was previously denied
      presentInRootViewController: true // iOS-only; If you're sure you're not presenting the (non embedded) scanner in a modal, or are experiencing issues with fi. the navigationbar, set this to 'true' and see if it works better for your app (default false).
    }).then((result) => {
        // Note that this Promise is never invoked when a 'continuousScanCallback' function is provided
        // alert({
        //   title: "Scan result",
        //   message: "Format: " + result.format + ",\nValue: " + result.text,
        //   okButtonText: "OK"
        // });
        console.log(result);
        console.log("Format: " + result.format + ",\nValue: " + result.text);
      }, (errorMessage) => {
        console.log("No scan. " + errorMessage);
      }
    );
  }

  sendInvite() {
    this.isLoading = true;
    var user = {
      // "invitedByUserId":"000001",
      "user": {
        "firstName":"User",
        // "lastName":"PO",
        "email":"rohanrevanthsagar@gmail.com",
        "mobileNumber":"8473893929"
      },
      // "tenant": {
      //   "tenantKey":"suitepaws"
      // }
    };
    this.appService.inviteUser(user).subscribe(
      res => {
        this.isLoading = false;
        console.log(res);
          this.alertService.showSuccess('Success', 'Invite sent.', 'Next').then(() => {
            
          });
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  getAppointments() {  
    this.isLoading = true;
    var status;
    if(this.type == 'check-in') {
      status = "CONFIRMED";
    }
    if(this.type == 'check-out') {
      status = "CHECKIN";
    }  
    this.appService.getUserAppointments(this.name, status)
    .subscribe(
      (res : any) => {
        // this.isLoading = false;
        console.log(res);
        var userAppointments = res["data"] ? res["data"]["content"] ? res["data"]["content"] : [] : [];
        let navigationExtras: NavigationExtras = {
          queryParams: {
            "user" : this.name,
            "type" : this.type,
            "appointment" : JSON.stringify(userAppointments)
          }
        };
        if(userAppointments.length == 0) {
          if(this.type == 'check-in') {
            this.getUserData();
          } else {
            this.showNoAppointmentPopup();
            this.isLoading = false;
          }
        }
        if(userAppointments.length > 1) {
          this.router.navigate(['appointments-list'], navigationExtras);          
        }
        if(userAppointments.length == 1) {
          this.router.navigate(['appointment-detail'], navigationExtras);          
        }
      },
      (error : any) => {
        this.isLoading = false;
        console.log("error", error);
      }
    );
  }

  getUserData() {
    this.isLoading = true;
    this.appService.getUserData(this.name).subscribe(
      (res : any) => {
        console.log("user", res);
        this.routeToMemberships(res["data"]);
      },
      (error : any) => {
        console.log(error);
        this.isLoading = false;
      }
    );
  }

  showNoAppointmentPopup() {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "OKAY",
        description : "You don't have any appointments to " + this.type + " today.",
        header : "",
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
      });
  }

  routeToMemberships(user) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "user" : JSON.stringify(user),
        // "type" : this.type,
      }
    };
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "OKAY",
        description : "You don't have any appointments to " + this.type + " today. Checking for memberships...",
        header : "",
        image:"~/assets/information.png"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        setTimeout(() => {
          this.isLoading = false;
          this.router.navigate(['memberships'], navigationExtras);      
        }, 300);
      });
  }

  goBack() {
    this.router.navigate(['flow-select']);
  }

  
}
