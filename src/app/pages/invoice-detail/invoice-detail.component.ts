import { Component, OnInit, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import * as moment from 'moment-timezone';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { TipPopupComponent } from "../../components/tip-popup/tip-popup.component";
import { RouterExtensions } from "@nativescript/angular";
import { AlertService } from '../../services/alert.service';

import { registerElement } from '@nativescript/angular';
import { ImageSource } from '@nativescript/core';
import { DrawingPad } from '@nativescript-community/drawingpad';
import { knownFolders, Folder, File, path, FileSystemEntity } from "@nativescript/core/file-system";

registerElement('DrawingPad', () => DrawingPad);

@Component({
  selector: 'app-invoice-detail',
  templateUrl: './invoice-detail.component.html',
  styleUrls: ['./invoice-detail.component.css']
})
export class InvoiceDetailComponent implements OnInit {

  @ViewChild('DrawingPad') DrawingPad_: ElementRef;

  listOfCards : any = [];
  appointmentObj : any;
  invoiceDetails : any;
  invoiceDetails_original : any;
  type : any;
  tipAmount = 0;
  tipValue = 0;
  isSigned = false;

  isLoading = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alertService : AlertService,
    public appService : AppService,
    private routerExtensions: RouterExtensions,
    private viewContainerRef: ViewContainerRef,
    private modalService: ModalDialogService, 
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params.type);
      this.type = params.type;
      this.appointmentObj = JSON.parse(params.appointment);
      this. getInvoiceDetails();
      
    });
  }

  getInvoiceDetails() {
    this.isLoading = true;
    this.appService.getAppointmentInvoice(this.appointmentObj.appointmentKey,this.appointmentObj.location.locationKey)
    .subscribe(
      (res : any) => {
      this.isLoading = false;
      console.log(res);
        if (res.data)
        {
          console.log(res.data);
          this.invoiceDetails = res["data"];
          console.log("invoice", this.invoiceDetails);
          this.invoiceDetails_original = JSON.stringify(res["data"]);
          this.tipValue = this.invoiceDetails.totalTipAmount ? this.invoiceDetails.totalTipAmount : 0;
          var balanceDueWithPreAddedTip = this.invoiceDetails.balanceDue + this.tipValue;
          this.invoiceDetails["balanceDue"] = balanceDueWithPreAddedTip;
          this.invoiceDetails["balanceDueToDisplay"] = this.invoiceDetails.balanceDue;     
          if (this.invoiceDetails.type=='ESTIMATION' && this.appointmentObj.status=='CHECKIN') {
              this.saveInvoice(res.data);
          }
        }
      
      },
      (error : any) => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  

  saveInvoice(invoice:any) {
    invoice.saveInvoice = true;
    this.isLoading = true;
    console.log("called save invoice")
    console.log(invoice);
    this.appService.saveInvoice(this.appointmentObj.appointmentKey,this.appointmentObj.location.locationKey, invoice)
    .subscribe(
      (res : any) => {
        console.log(res);
        this.isLoading = false;
        if (res.data)
        {
          this.invoiceDetails = res["data"];
          this.invoiceDetails_original = JSON.stringify(res["data"]);
          this.tipValue = this.invoiceDetails.totalTipAmount ? this.invoiceDetails.totalTipAmount : 0;
          var balanceDueWithPreAddedTip = this.invoiceDetails.balanceDue + this.tipValue;
          this.invoiceDetails["balanceDue"] = balanceDueWithPreAddedTip;
          this.invoiceDetails["balanceDueToDisplay"] = this.invoiceDetails.balanceDue;   
        }
      },
      (error : any) => {
        this.isLoading = false;
        console.log("error", error);
      }
    );
  }

  showPayNowButton() {
    if(this.invoiceDetails.balanceDue > 0) {
      if(this.invoiceDetails.status == 'OPEN' && this.appointmentObj.status=='CHECKIN') {
        return true;
      }
    }
    return false;
  }


  editTip() {
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "tip-popup",
        okText : "Done",
        cancelText : "Cancel",
        description : "We hope you liked our service. Please leave a tip to show your appreciation.",
      }
    };
    this.modalService.showModal(TipPopupComponent, options)    
      .then((result: any) => {
        console.log("result", result);
        if(result || result == 0) {
          this.tipAmount = Number(result);
          this.tipValue = (this.invoiceDetails.payableAmount * this.tipAmount / 100);
          this.invoiceDetails.balanceDueToDisplay = this.invoiceDetails.balanceDue + this.tipValue;
          this.saveTipAmount();
        }
      });
  }

  saveTipAmount() {
    var body = {"list":[{"tipAmount":this.tipValue}]};
    this.isLoading = true;
    this.appService.saveTip(body, this.invoiceDetails.estimateNumber).subscribe(
      (res : any) => {
        this.isLoading = false;
        this.getInvoiceDetails();
      },
      (error : any) => {
        this.isLoading = false;
        console.log("error", error);
      }
    );
  }

  payNow() {
    this.isLoading = true;
    this.appService.getSavedCards().subscribe(
      res => {
        this.isLoading = false;
        var cards = res["list"];
        if(cards.length > 0) {
          this.appointmentObj["cards"] = cards;
          this.appointmentObj["invoiceDetails"] = this.invoiceDetails;
          let navigationExtras: NavigationExtras = {
            queryParams: {
              "type" : this.type,
              "appointment" : JSON.stringify(this.appointmentObj)
            }
          };
          this.router.navigate(['cards'], navigationExtras);
        } else {
          this.payAmount()
        }
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
      }
    );
  }

  payAmount() {
      this.isLoading = true;
      var body = { 
        "paymentType": "Credit", 
        "transactionType": "Sale", 
        "applicationReturnUrl": this.appService.paymentReturnUrl, 
        "paymentMode": "CARD_NOT_PRESENT", 
        "amount": this.invoiceDetails.balanceDueToDisplay,
        "loggedInUser" : {
          "userKey" : this.appointmentObj.user.userKey
        }
      }
      console.log(body);
      this.appService.getPaymentLink(body, this.invoiceDetails.estimateNumber).subscribe(
        (res : any) => {
          this.isLoading = false;
          var paymentDetails = res["data"];
          let navigationExtras: NavigationExtras = {
            queryParams: {
              "type" : this.type,
              "appointment" : JSON.stringify(this.appointmentObj),
              "paymentUrl" : paymentDetails.checkoutUrl
            }
          };
          this.router.navigate(['payment'], navigationExtras);
        },
        (error : any) => {
          this.isLoading = false;
          console.log("error", error);
        }
      );
  }

  pleaseSignPopup() {
    this.alertService.showError('Error', 'Please sign to continue.', 'Okay').then(() => {
      
    });
  }

  goBack() {
    // this.router.navigate(['flow-select']);
    this.routerExtensions.back();
    
  }

  goNext() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify(this.appointmentObj)
      }
    };
    if(this.type==='check-in') {
      if(!this.isSigned) {
        this.pleaseSignPopup();
        return;
      }
      this.getMyDrawing();
    }
    this.router.navigate(['pet-belongings'], navigationExtras);
  }

  getReadableDateShort(date:any) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('MMM Do YYYY');
  }

  getMyDrawing() {
    this.isLoading = true;
    // get reference to the drawing pad
    const pad = this.DrawingPad_.nativeElement;

    // then get the drawing (Bitmap on Android) of the drawingpad
    let drawingImage;
    pad.getDrawing().then((data : any) => {
        console.log(data);
        // At this point you have a native image (Bitmap on Android or UIImage on iOS)
        // so lets convert to a NS Image using the ImageSource
        const image = new ImageSource(data); // this can be set as the `src` of an `Image` inside your NS
        drawingImage = image; // to set the src of an Image if needed.
        
        const folder = knownFolders.documents().path;
        const fileName = new Date().getTime() + ".jpg"; 
        const path_ = path.join(folder, fileName);
        const saved = image.saveToFile(path_, 'jpg');
        const file = File.fromPath(path_);

        // now you might want a base64 version of the image
        const base64imageString = image.toBase64String('jpg'); // if you need it as base64
        console.log('::IMG_BASE64::', base64imageString);
        this.getUploadUrl(file);
      },
      (err : any) => {
        console.log(err);
        this.isLoading = false;
      }
    );
  }

  getUploadUrl(image : any) {
    this.appService.getSignatureImageUploadUrl(this.appointmentObj.appointmentKey, image).subscribe(
      (res : any) => {
          console.log(res);
          this.appService.uploadImageToAws(res["data"].url, image);
          this.isLoading = false;
      },
      (error : any) => {
        this.isLoading = false;
        console.log("error", error);
      }
    );
  }

  onSignatureTouch(event : any) {
    console.log(event.action);
    if(event.action == 'move') {
      this.isSigned = true;
    }
  }

  clearMyDrawing() {
    const pad = this.DrawingPad_.nativeElement;
    pad.clearDrawing();
    this.isSigned = false;
  }
}
