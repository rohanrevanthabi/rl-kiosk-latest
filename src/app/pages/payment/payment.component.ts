import { Component, OnInit, ViewContainerRef, ElementRef, ViewChild, ChangeDetectorRef, NgZone, AfterViewInit } from '@angular/core';
import { AppService } from '../../services/app.service';
import { StorageHelper } from '../../services/storage.helper';
import { UtilityService } from "../../services/utility.service";
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertService } from '../../services/alert.service';

import { ModalDialogService, ModalDialogOptions } from "@nativescript/angular";
import { GeneralModalComponent } from "../../components/general-modal/general-modal.component";

import * as application from "@nativescript/core/application";
import { WebView, LoadEventData } from "@nativescript/core/ui/web-view";
let webViewInterfaceModule = require('nativescript-webview-interface');
import { device, screen, isAndroid, isIOS } from "@nativescript/core/platform";
import { Page } from '@nativescript/core/ui/page';
import { RouterExtensions } from "@nativescript/angular";

declare var android: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  isLoading = false;
  isWebview : any = true;

  routeAfterAddingCard = false;

  type : any;
  appointment : any;
  isWebViewDisplayed = false;
  @ViewChild('webView') webView: ElementRef;

  paymentWebViewInterface : any;

  constructor(
    public appService : AppService,
    private viewContainerRef: ViewContainerRef,
    private alertService : AlertService,
    private changeDetectorRef: ChangeDetectorRef,
    private modalService: ModalDialogService, 
    private router: Router,
    private routerExtensions: RouterExtensions,
    private route: ActivatedRoute,
  ) { 
    
  }

  ngOnInit(): void {
 
  }

  ngAfterViewInit() {
    console.log("ngAfterViewInit");
    this.route.queryParams.subscribe(params => {
      console.log(params);
      if (!this.isWebViewDisplayed)
      {

        this.type = params.type;
        this.appointment = JSON.parse(params.appointment);
        this.routeAfterAddingCard = params.routeAfterAddingCard;
        this.setupWebViewInterface(params.paymentUrl);
      }
     

      // this.setupWebViewInterface("https://www.google.com");
    });
  }

  /**
   * Initializes webViewInterface for communication between webview and android/ios
   */
  private setupWebViewInterface(checkoutUrl : any) {
    this.isWebview = true;
    // this.showPage = false;

    let webView: WebView = this.webView.nativeElement;

    this.paymentWebViewInterface = new webViewInterfaceModule.WebViewInterface(webView, checkoutUrl);
    
    this.listenWebViewEvents();
  }

  onWebViewLoaded(event : any) {
    const page: Page = <Page> event.object.page;
    const vm = page.bindingContext;
    const webview: WebView = <WebView> event.object;

    webview.on('loadFinished', (args: LoadEventData) => {
        let message = "";
        if (!args.error) {
            message = `WebView finished loading of ${args.url}`;
        } else {
            message = `Error loading ${args.url} : ${args.error}`;
        }
        console.log(`WebView message - ${message}`);

        if (isAndroid) {
            class MyWebChromeClient extends android.webkit.WebChromeClient
            {
                constructor()
                {
                    super();
                    return global.__native(this);
                }
                init()
                {
                    return global.__native(this);
                }
                // onJsAlert( webview: android.webkit.WebView, url: string, msg: string, result: android.webkit.JsResult ) : boolean
                onJsAlert( webview: any, url: string, msg: string, result: any ) : boolean
                {
                    result.confirm();                    
                    return true;
                }
            }
                        
            (<WebView>args.object).android.getSettings().setJavaScriptEnabled( true );
            (<WebView>args.object).android.setWebChromeClient( new MyWebChromeClient() );
        } else {

        }
    });
  }

  /**
   * Handles any event/command emitted by webview.
   */
  private listenWebViewEvents() {
    this.paymentWebViewInterface.on('paymentEmitter', (resp : any) => {
      if (resp.status=="APPROVED")
      {
        if(this.routeAfterAddingCard) {
          this.goBack();
        } else {
          this.updateCheckoutAppointment();
        }
      }
      else{
          const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: false,
            context: {
              type : "default-popup",
              okText : "OKAY",
              description : "",
              header : "Payment is " + resp.status,
              image:"~/assets/information.png"
            },
            ios : {
              height : 450,
        width : 400
            }
          };
          this.modalService.showModal(GeneralModalComponent, options)    
            .then((result: string) => {
              console.log("result", result);
              if(result) {
                this.goBack();
              }
            });
      }
      this.changeDetectorRef.detectChanges();
    });
  }

  updateCheckoutAppointment() {
    this.isWebViewDisplayed = true;
    var request = {'status':'CHECKOUT','belongingsNotes':this.appointment.belongingsNotes,'reviewRequired':true};
        console.log(request);
        this.isLoading = true;
        this.appService.updateStatus(request, this.appointment.appointmentKey)
        .subscribe(
          (res : any) => {
            this.isLoading = false;
            console.log(res);
            this.alertService.showSuccess(this.appointment.appointmentDetails[0].pet.name + ' is now checked out!','Please proceed to front-desk.' , 'OK').then(() => {
              // this.router.navigate(['flow-select']);
              let navigationExtras: NavigationExtras = {
                queryParams: {
                  "appointment" : JSON.stringify(this.appointment),
                }
              };
              this.router.navigate(['review'], navigationExtras);
            });
          },
          (error : any) => {
            this.isLoading = false;
            console.log("error", error);
          }
        );
  }

  closeWebview() {
    console.log("closeWebview");
    const options: ModalDialogOptions = {
      viewContainerRef: this.viewContainerRef,
      fullscreen: false,
      context: {
        type : "default-popup",
        okText : "Confirm",
        cancelText : "Cancel",
        description : this.routeAfterAddingCard ? "Are you sure you want to cancel adding card?" : "Are you sure you want to cancel the payment?",
        // description : "Are you sure you want to cancel the payment?",
        header : "ALERT"
      },
      ios : {
        height : 450,
        width : 400
      }
    };
    this.modalService.showModal(GeneralModalComponent, options)    
      .then((result: string) => {
        console.log("result", result);
        if(result) {
          setTimeout(() => {
            this.goBack();
          }, 300);
        }
      });
  }

  goBack() {
    this.routerExtensions.back();
    // this.router.navigate(['flow-select']);
  }

}
