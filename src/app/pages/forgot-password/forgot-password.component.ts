import { Component, OnInit } from '@angular/core';
import { UtilityService } from "../../services/utility.service";
import { AppService } from '../../services/app.service';
import { AlertService } from '../../services/alert.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  isLoading = false;
  email : any;
  errorMsg = "";

  pause = false;

  constructor(
    private utilityService : UtilityService,
    private router: Router,
    public appService : AppService,
    private alertService : AlertService,
  ) { }

  ngOnInit(): void {

  }

  close(){
    this.router.navigate(['login']);

  }
  sendEmail() {
    if(!this.email || this.email == "") {
      this.errorMsg = "Email cannot be empty."
      return;
    }
    console.log("valid email",this.utilityService.isValidEmail(this.email));
    if(!this.utilityService.isValidEmail(this.email)) {
      this.errorMsg = "Please enter a valid email."
      return;
    }

    this.isLoading = true;
    this.errorMsg = "";
    this.appService.forgotPassword(this.email).subscribe(
      (res : any) => {
        this.isLoading = false;
        if(res["data"]) {
          if(res["data"].responseCode == 1000) {
            this.alertService.showSuccess('Success', 'We have sent a link to your email to reset your password.', 'Close').then(() => {
              this.router.navigate(['login']);
            });
          } else {
            this.errorMsg = res["data"].message;
          }
        }
      },
      error => {
        console.log("error", error);
        this.isLoading = false;
        var errorMsg = error.error ? error.error.message ? error.error.message : "Unknown error." : "Unknown error.";
        this.alertService.showError('Error', errorMsg, 'Okay').then(() => {
          
        });
      }
    );
  }


  onScanResult(event) {
    console.log(event);
  }
}
