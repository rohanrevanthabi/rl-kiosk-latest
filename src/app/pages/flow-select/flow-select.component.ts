import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { AppService } from '../../services/app.service';
import { StorageHelper } from '../../services/storage.helper';

@Component({
  selector: 'app-flow-select',
  templateUrl: './flow-select.component.html',
  styleUrls: ['./flow-select.component.css']
})
export class FlowSelectComponent implements OnInit {

  storageService = new StorageHelper();
  disableHeader = true;

  isLoading = false;

  constructor(
    private router: Router,
    public appService : AppService,
  ) { 
    this.storageService = new StorageHelper();
  }

  tenantLogo = "";
  
  ngOnInit(): void {
    this.getTenantDetail();
  }

  getTenantDetail() {
    this.isLoading = true;
    this.appService.getTenantData()
    .subscribe(
      (res : any) => {
        console.log(res);
        this.isLoading = false;
        if (res.data) {
            this.tenantLogo = res.data.TenantLogo ? res.data.TenantLogo : "https://www.runloyal.com/wp-content/uploads/2020/03/Logo-Transparency.png";
            this.storageService.setItem("tenantLogo", this.tenantLogo);
            console.log(this.tenantLogo);
        }
      },
      (error : any) => {
        this.isLoading = false;
        console.log("error", error);
      }
    );
  }
    
  goToUserEmail(flowType : string) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : flowType
      }
    };
    this.router.navigate(['user-email'], navigationExtras);
  }
}
