import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AppService } from '../../services/app.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-appointments-list',
  templateUrl: './appointments-list.component.html',
  styleUrls: ['./appointments-list.component.css']
})
export class AppointmentsListComponent implements OnInit {

  appointmentsList : any = [];

  type : any;

  constructor(
    public appService : AppService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      console.log(params);
      
      this.type = params.type;
      // console.log(this.appointmentsList.length);
      this.appointmentsList = JSON.parse(params.appointment);
      console.log(this.appointmentsList.length);
    });
  }

  getReadableDate(date : any) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('MMMM Do YYYY h:mm A');
  } 

  getMonthYear(date : any) {
    if(!date) {
      return "";
    }
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    return monthNames[new Date(date).getMonth()] + " " + new Date(date).getFullYear();
  }

  getDay(date : any) {
    if(!date) {
      return "";
    }
    var m = moment(date);
    return m.format('DD');
  } 

  getAppointmentDescription(appointment : any) {
    if(appointment.status == "PENDING") {
      return "You have a pending appointment for " + (appointment.appointmentDetails.length == 1 ? appointment.appointmentDetails[0].pet.name : 'your pets.');
    }
    if(appointment.status == "STARTED" || 
      appointment.status == "CONFIRMED" || 
      appointment.status == "ON_THE_WAY" || 
      appointment.status == "CHECKIN") {
      return "You have an appointment for " + (appointment.appointmentDetails.length == 1 ? appointment.appointmentDetails[0].pet.name : 'your pets.');
    }
    if(appointment.status == "NOT_SHOWED" || 
      appointment.status == "COMPLETED" || 
      appointment.status == "REVIEW_REQUESTED" || 
      appointment.status == "EXPIRED" || 
      appointment.status == "NO_SHOW" || 
      appointment.status == "CLOSED" || 
      appointment.status == "CANCELLED") {
      return "You had an appointment for " + (appointment.appointmentDetails.length == 1 ? appointment.appointmentDetails[0].pet.name : 'your pets.');
    }
    return "";
  }

  selectAppointment(appointment : any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "type" : this.type,
        "appointment" : JSON.stringify([appointment])
      }
    };
    this.router.navigate(['appointment-detail'], navigationExtras);          
  }

  getDate(date : any) {
    if(!date) {
      return "";
    }
    return new Date(date).toDateString();
  }

 


}
